#include <mutex> // Boost Beast bug workaround

#include "dcboost/intents.hpp"
#include "dcboost/discord.hpp"

#include <exception>
#include <memory>
#include <boost/asio/awaitable.hpp>
#include <json/json.h>



namespace ChatFuse {
namespace Discord {
asio::awaitable<void> Client::intentHandlerLauncher(std::string intent, Json::Value json) {
    try {
        auto i = shared_from_this();

        co_await intentHandler(std::move(intent), std::move(json));
    } catch (std::exception& e) {
        logger.log(QLog::Loglevel::error, std::string("Exception in intent handling: ")+e.what());
    }
}
}
}
