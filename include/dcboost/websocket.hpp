namespace ChatFuse {
namespace Discord {
class WebSocket;
}
}

#ifndef _WEBSOCKET_HPP
#define _WEBSOCKET_HPP

#include "ns_base.hpp"
#include "discord.hpp"
#include "stoppable.hpp"

#include <json/json.h>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>
#include <boost/beast/websocket/stream.hpp>
#include <memory>



namespace ChatFuse {
namespace Discord {
enum class ws_op {
    dispatch = 0,
    heartbeat = 1,
    identify = 2,
    presence_update = 3,
    voice_state_update = 4,
    resume = 6,
    reconnect = 7,
    request_guild_members = 8,
    invalid_session = 9,
    hello = 10,
    heartbeat_ack = 11
};


/*
 * This class provides a little wrapper around the
 * beast websocket. Additionally, it sends heartbeats.
 */
class WebSocket : public AsyncHelpers::Stoppable {
    Client& main;
    using streamT = beast::websocket::stream<beast::ssl_stream<asio::ip::tcp::socket>>;
    std::unique_ptr<streamT> stream = nullptr;
    asio::ssl::context ssl_ctx;
    asio::ip::tcp::resolver::results_type::endpoint_type endpoint;
    time_t heartbeat_interval = 0;
    int lastSeqNo = -1;

    /*
     * Sends heartbeats in the interval
     * provided by Discord.
     * Without this, connection won't
     * stay alive for long.
     */
    asio::awaitable<void> heartbeat_send();

public:
    WebSocket(Client& main) :
         main(main), ssl_ctx(asio::ssl::context::sslv23) {}

    /*
     * Returns if the WebSocket connection
     * is currently alive.
     */
    auto isConnected() {
        return bool(stream);
    }

    /*
     * Sends a plain string to the WebSocket.
     */
    asio::awaitable<size_t> send_raw(std::string_view str);

    /*
     * Sends a message in JSON format to
     * the WebSocket.
     */
    asio::awaitable<void> send_json(const Json::Value& json) {
        co_await send_raw(json.toStyledString());
    }

    /*
     * Connects to the WebSocket and
     * kicks off the heartbeat if
     * required.
     */
    asio::awaitable<void> connect();
    /*
     * Receives exactly one JSON message
     * from the WebSocket.
     * Returns a Json::nullValue if data
     * was invalid.
     */
    asio::awaitable<Json::Value> recv_json();
};
}
}
#endif
