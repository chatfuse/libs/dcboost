namespace ChatFuse {
namespace Discord {
class Snowflake;
}
}

#ifndef _SNOWFLAKE_HPP
#define _SNOWFLAKE_HPP
#include "ns_base.hpp"

#include <string>
#include <json/value.h>



namespace ChatFuse {
namespace Discord {
class Snowflake {
    uint64_t val;

    template<typename intT = int>
    static inline
    auto crossplatform_stou(const std::string& str) {
        intT fres = 0;
        // Iterate through string backwards
        bool first = true;
        for (const auto character : str) {
            if (first) [[unlikely]] {
                first = false;
            } else {
                fres *= 10;
            }
            fres += character - '0';
        }
        // Return result
        return fres;
    }

public:
    Snowflake()
        : val(0) {}
    Snowflake(uint64_t value)
        : val(value) {}
    Snowflake(const std::string& value)
        : val(crossplatform_stou<uint64_t>(value)) {}
    Snowflake(const Json::Value& value) {
        if (value.isString()) {
            val = crossplatform_stou<uint64_t>(value.asString());
        } else {
            val = 0;
        }
    }

    operator std::string()  const {
        return std::to_string(val);
    }
    operator uint64_t() const {
        return val;
    }
    operator Json::Value() const {
        return std::string(*this);
    }
    operator bool() const {
        return val;
    }

    std::string str() const {
        return *this;
    }
    uint64_t uint() const {
        return *this;
    }
    bool empty() const {
        return !*this;
    }

    bool operator==(const Snowflake& o) const {
        return o.val == this->val;
    }
};
}
}

namespace std {
  template <>
  struct hash<ChatFuse::Discord::Snowflake>
  {
    std::size_t operator()(const ChatFuse::Discord::Snowflake& k) const {
      return k.uint();
    }
  };

}
#endif
