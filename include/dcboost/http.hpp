namespace ChatFuse {
namespace Discord {
class APIClient;
class Client; // To avoid some include cycling
}
}

#ifndef _HTTP_HPP
#define _HTTP_HPP
#include "ns_base.hpp"
#include "settings.hpp"

#include <string>
#include <chrono>
#include <exception>
#include <stdexcept>
#include <mutex> // Boost Beast bug workaround
#include <json/json.h>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/beast/http/verb.hpp>



namespace ChatFuse {
namespace Discord {
class APIClient {
    Client& main;
    boost::asio::ip::tcp::resolver resolver;
    boost::asio::ssl::context ssl_ctx{boost::asio::ssl::context::tlsv12_client};

public:
    struct Exception : public Discord::Exception {
        using Discord::Exception::Exception;
    };

    std::chrono::minutes timeout = std::chrono::minutes(2);

    APIClient(Client& main);

    asio::awaitable<Json::Value> call(beast::http::verb const method, const std::string& path, const Json::Value& data = Json::nullValue);
};
}
}
#endif
