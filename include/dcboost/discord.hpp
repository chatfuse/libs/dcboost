namespace ChatFuse {
namespace Discord {
class Client;
}
}

#ifndef _MAIN_HPP
#define _MAIN_HPP
#include "ns_base.hpp"
#include "settings.hpp"
#include "intents.hpp"
#include "websocket.hpp"
#include "http.hpp"
#include "stoppable.hpp"
#include "logger.hpp"

#include <string>
#include <memory>
#include <json/json.h>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>
#include <boost/asio/steady_timer.hpp>



namespace ChatFuse {
namespace Discord {
/*
 * This is basically the core of it all:
 * It uses the Connection class to connect
 * to be websocket.
 */
class Client : public std::enable_shared_from_this<Client>, AsyncHelpers::Stoppable {
    friend WebSocket;
    friend APIClient;

    asio::awaitable<void> ws_run();
    asio::awaitable<void> intentHandlerLauncher(std::string intent, Json::Value json);

protected:
    asio::io_context& io;
    QLog::Logger<Client> logger;
    Json::Value properties;

public:
    WebSocket c;
    APIClient api;
    Settings settings;

    Client(asio::io_context& io, Settings settings = {}) : io(io), c(*this), api(*this), settings(std::move(settings)) {
        logger.inst_ptr = this;
    }

    /*
     * Stops all async loops.
     */
    void stop() {
        Stoppable::stop();
        c.stop();
    }
    /*
     * Starts and detaches WebSocket
     * connection.
     */
    void detach() {
        asio::co_spawn(io, ws_run(), boost::asio::detached);
    }

    QLog::Loglevel& loglevel() {
        return logger.level;
    }

    asio::awaitable<void> asyncSleep(time_t milliseconds) {
        co_await asio::steady_timer(io, asio::chrono::milliseconds(milliseconds)).async_wait(asio::use_awaitable);
    }

    virtual asio::awaitable<void> intentHandler(std::string intent, Json::Value data) = 0;
};
}
}
#endif
