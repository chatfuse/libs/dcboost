namespace ChatFuse {
namespace Discord {
struct IntentHandlers;
}
}

#ifndef _INTENTS_HPP
#define _INTENTS_HPP
#include "ns_base.hpp"

#include <functional>
#include <mutex> // Boost Beast bug workaround
#include <boost/asio/awaitable.hpp>

namespace ChatFuse {
namespace Discord {
namespace intents {
    constexpr uint16_t
        none = 0,
        guilds = 1u << 0u,
        guild_members = 1u << 1u,
        guild_bans = 1u << 2u,
        guild_emojis = 1u << 3u,
        guild_integrations = 1u << 4u,
        guild_webhooks = 1u << 5u,
        guild_invites = 1u << 6u,
        guild_voice_states = 1u << 7u,
        guild_presences = 1u << 8u,
        guild_messages = 1u << 9u,
        guild_message_reactions = 1u << 10u,
        guild_message_typing = 1u << 11u,
        direct_messages = 1u << 12u,
        direct_message_reactions = 1u << 13u,
        direct_message_typing = 1u << 14u,
        all = 0xffff;
};
}
}
#endif
