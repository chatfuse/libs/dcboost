namespace ChatFuse {
namespace Discord {
struct Settings;
}
}

#ifndef _SETTINGS_HPP
#define _SETTINGS_HPP
#include <string>



namespace ChatFuse {
namespace Discord {
/*
 * This struct holds some important settings
 */
struct Settings {
    std::string bot_token;
    int intents = 0;
    bool is_bot = true;
};
}
}
#endif
