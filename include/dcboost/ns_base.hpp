#ifndef _NS_BASE_HPP
#define _NS_BASE_HPP
#ifndef BOOST_ASIO_HAS_CO_AWAIT
#ifdef CDLNG_CORO_LEGACY_FIX
#define BOOST_ASIO_HAS_STD_COROUTINE
#endif
#define BOOST_ASIO_HAS_CO_AWAIT
#endif
#include <string>
#include <stdexcept>
#include <utility>



namespace boost {
namespace asio {}
namespace beast {}
}

namespace ChatFuse {
namespace Discord {
struct Exception : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

namespace asio = boost::asio;
namespace beast = boost::beast;

/*
 * These are the details used to connect
 * to the Discord API/WebSocket.
 * Changing these is possible but not
 * recommended unless you exactly know
 * what you are doing.
 * Don't open bug reports with these
 * details changed.
 */
extern const std::string ws_host,
                         api_host,
                         api_version,
                         api_path,
                         browser_user_agent,
                         browser_version,
                         browser_client_build_number;
}
}
#endif
