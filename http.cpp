#include "dcboost/http.hpp"
#include "dcboost/discord.hpp"
#include "dcboost/macaron_base64.hpp"

#include <string>
#include <string_view>
#include <json/json.h>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/core/detail/base64.hpp>



namespace ChatFuse {
namespace Discord {
const std::string api_host = "discord.com",
                  api_version = "9",
                  api_path = "/api/v"+api_version,
                  browser_user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0",
                  browser_version = "109.0",
                  browser_client_build_number = "166505";

APIClient::APIClient(Client& main)
    : resolver(main.io), main(main) {}

asio::awaitable<Json::Value> APIClient::call(beast::http::verb const method, const std::string& path, const Json::Value& data) {
    auto i = main.shared_from_this();

    // These objects perform our I/O
    beast::ssl_stream<beast::tcp_stream> stream(main.io, ssl_ctx);

    // Set SNI Hostname (many hosts need this to handshake successfully)
    if(!SSL_set_tlsext_host_name(stream.native_handle(), api_host.c_str())) {
        boost::system::error_code ec;
        ec.assign(static_cast<int>(::ERR_get_error()), asio::error::get_ssl_category());
        std::cerr << ec.message() << "\n";
        throw Exception("Failed to set SNI hostname: "+ec.message());
    }

    // Look up the domain name
    auto results = co_await resolver.async_resolve(api_host, "443", asio::use_awaitable);

    // Set the timeout
    beast::get_lowest_layer(stream).expires_after(timeout);

    // Make the connection on the IP address we get from a lookup
    co_await get_lowest_layer(stream).async_connect(results, asio::use_awaitable);

    // Set the timeout
    beast::get_lowest_layer(stream).expires_after(timeout);

    // Perform the SSL handshake
    co_await stream.async_handshake(asio::ssl::stream_base::client, asio::use_awaitable);

    // Set up an HTTP GET request message
    beast::http::request<beast::http::string_body> req;
    req.version(11);
    req.method(method);
    req.target(api_path+path);
    req.set(beast::http::field::host, api_host);
    req.set(beast::http::field::user_agent, main.settings.is_bot?BOOST_BEAST_VERSION_STRING:browser_user_agent);
    req.set(beast::http::field::authorization, (main.settings.is_bot?"Bot ":"")+main.settings.bot_token);
    req.set("X-RateLimit-Precision", "millisecond");
    if (!main.settings.is_bot) {
        req.set("Alt-Used", api_host);
        req.set("Origin", "https://"+api_host);
        req.set("Referer", "https://"+api_host+"/app");
        req.set("Sec-Fetch-Dest", "empty");
        req.set("Sec-Fetch-Mode", "cors");
        req.set("Sec-Fetch-Site", "same-origin");
        req.set("X-Debug-Options", "bugReporterEnabled");
        req.set("X-Discord-Locale", "en-US");
        req.set("X-Super-Properties", macaron::Base64::Encode(Json::writeString(Json::StreamWriterBuilder(), (main.properties))));
    }
    if (!data.isNull()) {
        req.set(beast::http::field::content_type, "application/json");
        req.body() = data.toStyledString();
        req.prepare_payload();
    }

    // Set the timeout
    beast::get_lowest_layer(stream).expires_after(timeout);

    // Send the HTTP request to the remote host
    co_await beast::http::async_write(stream, req, asio::use_awaitable);

    // This buffer is used for reading and must be persisted
    beast::flat_buffer b;

    // This is the object that holds the servers response
    beast::http::response<beast::http::dynamic_body> resp;

    // Receive the HTTP response
    co_await beast::http::async_read(stream, b, resp, asio::use_awaitable);


    // Deserialize body
    Json::Value fres = Json::nullValue;
    if (resp.body().size() != 0) {
        Json::Reader().parse(std::string(reinterpret_cast<const char*>((*resp.body().cdata().begin()).data()), resp.body().size()), fres);
    }

    // Get status code
    auto status = resp.result_int();

    // Check for error
    if (status == 429) {
        // Ratelimit
        main.logger.log(QLog::Loglevel::warn, "Ratelimit was hit at "+path);
        co_await main.asyncSleep(fres["retry_after"].asFloat()*1000);
        co_return co_await call(method, path, data); // Retry
    } else if (status < 200 || status >= 300) {
        throw Exception("Discord returned error code "+std::to_string(status)+" with the following body:\n"+fres.toStyledString());
    }

    // Set the timeout
    beast::get_lowest_layer(stream).expires_after(timeout);

    // Shutdown stream gracefully
    try {
        co_await stream.async_shutdown(asio::use_awaitable);
    } catch (...) {
        // Exceptions during stream shutdown don't matter... Let's ignore them.
    }

    // Return final result
    co_return fres;
}
}
}
