#define BOOST_ASIO_HAS_CO_AWAIT
#include "dcboost/discord.hpp"

#ifndef NDEBUG
#   include <iostream>
#endif
#include <optional>
#include <json/json.h>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/asio/ssl/stream_base.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/beast/websocket/stream.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>



namespace ChatFuse {
namespace Discord {
const std::string ws_host = "gateway.discord.gg";


asio::awaitable<void> WebSocket::heartbeat_send() {
    auto i = main.shared_from_this();

    // Make heartbeat json
    Json::Value json;
    json["op"] = static_cast<unsigned int>(ws_op::heartbeat);
    auto& seq = json["d"];

    // Send heartbeats in a loop
    main.logger.log(QLog::Loglevel::info, "Websocket heartbeat started");
    while (isRunning()) {
        // Wait first
        co_await main.asyncSleep(heartbeat_interval);

        // Make json
        if (lastSeqNo < 0) {
            seq = Json::nullValue;
        } else {
            seq = lastSeqNo;
        }

        // Heartbeat
        main.logger.log(QLog::Loglevel::verbose, "Heartbeat sent");
        co_await send_json(json);
    }
    main.logger.log(QLog::Loglevel::info, "Websocket heartbeat stopped");
}

asio::awaitable<void> WebSocket::connect() {
    unstop();

    // Resolve hostname
    asio::ip::tcp::resolver r(main.io);
    auto hosts = co_await r.async_resolve(ws_host, "443", asio::use_awaitable);

    // Check resulution result
    if (hosts.empty()) {
        throw Exception("Failed to resolve WebSocket host");
    }
    endpoint = hosts->endpoint();

    // Connect
    stream = std::make_unique<streamT>(main.io, ssl_ctx);
    co_await stream->next_layer().next_layer().async_connect(endpoint, asio::use_awaitable);

    // SSL Handshake
    co_await stream->next_layer().async_handshake(asio::ssl::stream_base::client, asio::use_awaitable);

    // WebSocket configuration
    stream->set_option(
        beast::websocket::stream_base::timeout::suggested(
            beast::role_type::client));

    // Connect to WebSocket
    co_await stream->async_handshake(ws_host, "/?v="+api_version+"&encoding=json", asio::use_awaitable);

    // Get heartbeat interval
    bool need_heartbeat_kickoff = std::exchange(heartbeat_interval, (co_await recv_json())["d"]["heartbeat_interval"].asUInt()) == 0;

    // Kickoff heartbeat
    if (need_heartbeat_kickoff) {
        main.logger.log(QLog::Loglevel::debug, "Kicking off heartbeat");
        boost::asio::co_spawn(main.io, heartbeat_send(), boost::asio::detached);
    }
}

asio::awaitable<size_t> WebSocket::send_raw(std::string_view str) {
    main.logger.log(QLog::Loglevel::debug, "Client sends ws message");
    main.logger.log(QLog::Loglevel::trace, str);
    return stream->async_write(beast::net::buffer(str), asio::use_awaitable);
}

asio::awaitable<Json::Value> WebSocket::recv_json() {
    Json::Value fres = Json::nullValue;
    beast::flat_buffer buf;
    co_await stream->async_read(buf, asio::use_awaitable);
    auto str = std::string_view{reinterpret_cast<const char*>(buf.cdata().data()), buf.size()};
    main.logger.log(QLog::Loglevel::debug, "Client received ws message");;
    main.logger.log(QLog::Loglevel::trace, str);
    if (Json::Reader().parse(std::string(str), fres)) {
        auto& s = fres["s"];
        if (s.isInt()) {
            lastSeqNo = s.asInt();
        }
    }
    co_return fres;
}

asio::awaitable<void> Client::ws_run() {
    std::shared_ptr<Client> i;
    try {
        i = shared_from_this();
    } catch (std::bad_weak_ptr&) {
        logger.log(QLog::Loglevel::error, "My instance must be a shared pointer!");
        co_return;
    }
    unstop();

    // Authentication data
    std::string auth_raw;
    {
        Json::Value auth = Json::objectValue;
        auth["op"] = static_cast<unsigned int>(ws_op::identify);
        auto& d = auth["d"] = Json::objectValue;
        d["token"] = settings.bot_token;
        if (settings.is_bot) {
            d["intents"] = settings.intents;
        } else {
            d["compress"] = false;
            d["capabilities"] = 4093;
            auto& s = d["client_state"] = Json::objectValue;
            s["guild_version"] = Json::objectValue;
            s["highest_last_message_id"] = "0";
            s["read_state_version"] = 0;
            s["user_guild_settings_version"] = -1;
            s["user_settings_version"] = -1;
            s["private_channels_version"] = "0";
            s["api_code_version"] = 0;
            auto& p = d["presence"] = Json::objectValue;
            p["status"] = "unknown";
            p["since"] = 0;
            p["activities"] = Json::arrayValue;
            p["afk"] = false;
        }
        auto& p = d["properties"] = Json::objectValue;
        if (settings.is_bot) {
            p["$os"] = "linux";
            p["$browser"] = "CDLng";
            p["$device"] = "CDLng";
        } else {
            p["os"] = "Linux";
            p["browser"] = "Firefox";
            p["device"] = "";
            p["system_locale"] = "en-US";
            p["browser_user_agent"] = browser_user_agent;
            p["browser_version"] = browser_version;
            p["os_version"] = "";
            p["referrer"] = "";
            p["referring_domain"] = "";
            p["referrer_current"] = "";
            p["referring_domain_current"] = "";
            p["release_channel"] = "stable";
            p["client_build_number"] = browser_client_build_number;
            p["client_event_source"] = Json::nullValue;
        }
        auth_raw = auth.toStyledString();
        properties = std::move(p);
    }

    while (isRunning()) {
        try {
            // Connect
            co_await c.connect();

            // Authenticate
            co_await c.send_raw(auth_raw);

            logger.log(QLog::Loglevel::info, "Websocket connected");
            while (isRunning()) {
                auto data = co_await c.recv_json();
                switch (ws_op(data["op"].asUInt())) {
                case ws_op::heartbeat_ack: {
                    logger.log(QLog::Loglevel::verbose, "Heartbeat acknowledgement received");
                } break;
                [[likely]] case ws_op::dispatch: {
                    Json::String test;
                    auto t = data["t"].asString();
                    logger.log(QLog::Loglevel::verbose, "Intent received: "+t);
                    boost::asio::co_spawn(io, intentHandlerLauncher(std::move(t), std::move(data["d"])), boost::asio::detached);
                } break;
                [[unlikely]] case ws_op::invalid_session: {
                    throw Exception("Discord reported an invalid session. Is the token valid?");
                } break;
                default: {}
                }
            }
        } catch (std::exception& e) {
            logger.log(QLog::Loglevel::error, std::string("Exception in websocket connection: ")+e.what());
        }
    }
    logger.log(QLog::Loglevel::info, "Websocket stopped");
}
}
}
